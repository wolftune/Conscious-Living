# How to help improve FLO Conscious Living resources

We aspire to get constructive input from a community as wide and diverse as feasible. May all contributors have a smooth and supportive experience filled with satisfaction and appreciation. May we collaborate with integrity, aligned with CL and FLO values. May we accept trade-offs and imperfections along the way with forgiveness and grace.

We welcome all sorts of input and contributions: questions, comments, minor edits, formatting improvements, layout and rendering in nicer styles for print and web, illustrations, and more — including, of course, core conceptual inputs like improvements and additions to the overall teachings and practices.

Some ways to participate:

## Join the discussion group

We use Matrix.org (similar to Slack and Discord but fully-FLO) for open-ended discussion. Matrix.org works with various clients. Element is the most popular and robust default option. Because it focuses on security, it uses encryption. We suggest installing some persistent app and maintaining a reliable account (rather than opening in a browser and closing and then re-registering later).

The link for the CL space is: https://matrix.to/#/#conscious-living:matrix.org (for those who already have a Matrix.org account just use the end part: `#conscious-living:matrix.org`)

Within that space, the CL-principles room (#CL-principles:matrix.org) is for discussing the development of the CL resources here.

## Issue tickets at Codeberg

You can open and discuss [Issue tickets](https://codeberg.org/FLO-Conscience/Conscious-Living/issues) as a way to bring up suggestions, questions, or other feedback.

Incidentally, Codeberg focuses on software code, but the version-tracking and collaboration tools also work well for documentation such as this project. Codeberg is among the few such tools that prioritize ethics and principles (it is run by a German non-profit org).

As issues include discussion posts, they work as a basic forum for discussing CL. We ask that discussions still stay to a focused topic within each issue. Open-ended discussion can go in Matrix.

Each issue can have *labels* for issue type, priority, status, and so on. We may add labels for specific CL topics.

Within each issue, *checklists* can be added to the description to note specific steps.

We use *projects* to group together multiple issues that achieve an overall goal. Projects offer a kanban view, a board with different columns to organize and sort the issues.

We use *milestones* less. They offer a way to group issues for a time-frame just to plan work, with optional due-date optional.

## Amending the main files

We welcome suggested edits and amendments in *whatever* form and through *whatever* medium feels most comfortable. We wish for project norms to not discourage any participation.

For those comfortable with it, we appreciate changes submitted through Git at Codeberg.

## Licensing, and incorporating existing work

All submissions must use licensing compatible with CC BY-SA. By contributing any files or file changes, each contributor agrees to license their work under CC BY-SA 4.0 unless specifying another compatible licensing (which can include CC-BY, CC0, and anything fully public domain).

When submitting anything from another author, include all appropriate attributions.

## Files in the repository

Most files in the repo are plain text written in [Markdown](https://docs.codeberg.org/markdown/) (.md) format (which is also used in the issue tickets and at Matrix.org and many other places).

We urge (but do not absolutely require) that work of all sorts be done with **FLO software**. Appropriate FLO programs exist for text editing, audio, illustrations, animations, page layout, and more. Also, please use **FLO file formats**. Please ask the community for guidance as needed.

- File naming: use hyphens (not underscores or spaces) with capitalization generally following common rules for proper nouns and acronyms (and all-caps for special project files like README and CONTRIBUTING)
    - Keep file names unchanged when importing unchanged files from outside sources (names might change later, but the history of the original name will get included)

## "*TASKS*" marked in the files

Sometimes, an in-line note may be added where a task is needed to improve something. We mark that with `*TASK:*` which renders as *TASK:* 

*TASK:* decide whether these tasks should be made HTML comments so that they only show in plain text and not in rendered versions.

## Notes on using Git

### Git collaboration vs wiki style

Version control with Git is a lot like contributing to a wiki. With a wiki, everyone appends new versions to a single history. With Git, contributors can work on a separate "fork" (copy) of the repo or on different "branches" within a shared repo. Then, a "pull request" (PR) asks for the work to get merged in. This workflow makes it easier for people to work on multiple files at once and to review changes before they are published.

For those with no Git experience, it is probably easiest to start by using the Codeberg website. The more advanced approach involves installing Git on your local computer which makes it easier to work with your preferred tools.

The [Codeberg Docs](https://docs.codeberg.org/getting-started) cover basic steps to participating on the platform. They tend to assume some familiarity with Git and code-collaboration concepts. Please ask for help if you feel at all confused.

For any larger files like visual illustrations and audio, use [Git LFS](https://docs.codeberg.org/git/using-lfs/) (Large File Storage).

#### Web-UI workflow for beginning contributors

Contributors can "fork" the project without first getting any permissions. Changes made on branches at a fork can be submitted via a pull-request. Unfortunately, the Web UI is quite clunky (though doable) for keeping forks updated to be based on the latest version of the origin project (see <https://github.com/go-gitea/gitea/issues/20882> and the other issue it links to).

A slightly easier workflow for beginners: get in touch with project maintainers and ask to be added as a write-enabled collaborator. That way, you can make branches right at the original project (which will always start with the latest updates). Then, you will submit pull-requests from the working branch to the main branch (which is protected, so only the maintainers have permission to update it directly).

#### Suggested workflow for those who know Git

- Keep branch `main` on your fork matched to the origin
- Make changes on branches
- Keep history relatively clean by squashing and rebasing as appropriate, using good judgment for when distinct changes deserve to stay as separate commits for clearer history

#### On branches

The "development" branch includes files and notes to work through and slowly incorporate. Because it is messy, it may be difficult to make sense of. Anyone who really wants to help is welcome to participate, though doing so in conversation with Aaron is probably the best bet. Eventually, that branch will get removed, and this section of this CONTRIBUTING document will get updated appropriately. 

Because "development" has so many files that should never go to main *and* we want some parts to get to main as they are ready, the workflow for now looks like this:

1. within "development", clean up notes and get them in more usable structure or draft files
2. when specific files are clean enough to get toward merging to main, make a new branch based on "main" and "checkout" the specific files from development with `git checkout development [filename]`
3. merge that new focused working branch to main when ready
4. merge main back into development (so that it doesn't have old alternate versions of anything).

#### Git commit messages

Commit messages should be concise and specific to make history easily and meaningfully skimmable. Consider using the style of a news headline (one that best makes the point, not clickbait-style).
