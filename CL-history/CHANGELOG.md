# FLO-CL Changelog

Notable significant updates (and some contextual references):

## October 2024

In preparation for presentation at SeaGL 2024 announcing the FLO-CL project, completed set of FLO-related documents: Code of Conscious Conduct, Contributing guide, FLO explanations, Style guide for the project, various minor cleanups and edits

## Jan-March 2024

Expanded context-vs-content section, added full series of articles on *style*

## July-December 2023

Within Principles, added the first versions of Attune-Resolve-Review and Context-vs-Content articles including Constriction-and-Expansion, Metaphors, and Stories

## May 2023

Structure more settled: principles vs practice; and initial structure for topics within principles

## December 2022

Slowly building the repository as we process notes and organize materials into the structure

## August 2022

Initial work on determining core structure, describing the [CLG-to-CL](CLG-to-CL.md) process and history
