# Notes and Script from SeaGL 2024 FLO-CL Presentation

Aaron Wolf, core contributor to FLO-CL, gave the opening keynote presentation at SeaGL 2024 (the Seattle GNU/Linux conference).

The talk describes the story behind the FLO-CL project and emphasizes the importance of FLO process in general.

Talk listing: https://pretalx.seagl.org/2024/talk/TLTGUG

Title:

> FLO Conscience: Bringing free/libre/open mindsets and methods to all parts of life

Shorter abstract which just hints at enough to promote the talk:

> Free/Libre/Open (FLO) methods can apply well beyond software. This talk is the first big announcement of the FLO Conscious Living (CL) project.
>
> FLO-CL collects and organizes mental models and practices for living in more healthy, intentional, and ethical ways. As a FLO project, everyone is invited to participate in its evolution.

Longer abstract submitted as original description:

> Our world is overflowing with advice on leadership, communication, wellness, spirituality, and related topics. People keep adding yet more books, videos, podcasts, and blog posts to the zillions of lifetimes' worth of "content" we already have. FLO methods offer an alternative approach where we can better cooperate on building shared resources.
>
> FLO methods are prominent in software development and at Wikipedia, but they can apply to so much more.
>
> The FLO Conscious Living (CL) project shows the utility of FLO methods to other contexts. Also, the CL ideas themselves can inform everything from daily routines to relationships to creative work including software.
>
> This keynote will share the story of the FLO-CL project and give a brief tour of the current state and future plans. Being FLO, everyone is invited not only to use the resources but to *participate* in the ongoing evolution.
 

## Keynote speech

### Intro announcement

*Salt announces: Aaron is Snowdrift.coop co-founder, a community music teacher, and will be talking today about FLO-CL…*

### Time song

*play and sing "I Don't Have Time"*

### Why the song

I wrote the first version of that song back in 2018.
I recently updated it for Kite Guitar — ask me about that later…
I decided to share it here because this talk is partly about mindfulness and conscious-living.

How does all that relate to GNU/Linux?
Well, it relates to nearly *everything*…
But it's more than that…

This presentation is the first big public announcement of the FLO Conscious-Living project.

### FLO

To explain it, I'll start with the FLO part.

FLO means Free/Libre/Open. I say FLO rather than FOSS or FLOSS because it's not just for code. FLO can apply to music, writings, research, and more.

I imagine most of you already know this stuff, but I want to emphasize and clarify why FLO ideas matter and what they mean to me.

Partly, FLO means that the rights to use, adapt, and share are not limited to an exclusive group of people. It also means doing what we can to enable everyone to actually use those rights.

In a FLO context, anyone may make their own version of something. But that's not a *goal* in itself.
The *option* to fork serves as a check on power. We might fork a project to take it in new directions, or we might fork to preserve a project that is otherwise going in a direction we don't like.

But mainly, FLO methods help us work cooperatively.
They facilitate synthesis, consolidation, and curation. We can edit and simplify rather than just add and expand.

So, FLO methods focus on continually improving the *quality* of our shared resources instead of flooding the world with a massive *quantity* of information or apps or whatever other noise. 

And being more participatory, FLO projects blur the lines between authors and readers, between developers and users, between producers and consumers.

This does not erase distinctions between experts and novices.
But novices may still offer helpful input.
Thus, FLO methods support us all in living by the principle of *leaving things better than we found them*…

Many of us here have felt the satisfaction of finding a bug in some program and then fixing it for everyone.
We can rewrite a confusing sentence at Wikipedia.
If you see something missing in Open Street Map, you can update it.

A whole set of innovations made this possible.
Licensing, version control, and other tools and cultural norms.

Now, FLO does not mean everything is by committee.
We can still have individual opinions and styles.
Most FLO software projects have only one core maintainer.
FLO does not mean everything is like Wikipedia or takes a neutral point of view.

So, FLO is neither about individualism nor collectivism.
It's about *dynamism*.
It's about treating creative projects as adaptive living things.
And still, change isn't always rapid.
FLO projects can be conservative and cautious.
They just need effective methods for how to change and evolve as appropriate.

In the end, the key feature is *humans* freely making *conscious* decisions about the directions that we take our cultures and technologies.

### FLO challenges

So, why do we so often feel helpless about choosing the directions we are heading in as a society?

Well, many things are proprietary instead of FLO.
But what matters is not *only* who has rights
but also *who* has the resources to actually dedicate their time
and what incentives drive their decisions.

Today, most resources go toward whatever directions get the most profits for investors.
And that's not correlated with what's healthy for the world.

### Snowdrift

That's what led me to co-found Snowdrift.coop,
which is what I'm most known for in the free-software world.

Its focus is on *crowdmatching* as a better way to fund creative work.
The idea is for each of us to support worthy projects by pledging to give more if more people join us in giving.
And it could be working, if only we could somehow find enough funding to finish getting the platform itself launched…

But my focus here today is not on economics… 

### Snowdrift human-centric foundations

One thing I appreciate about the FLO movement is how we don't think of everything in just transactional terms.
And I was concerned not to lose that when introducing money to projects.

I never liked algorithmic methods for distributing resources.
Whether it's numbers of git commits or podcast episodes or blog articles or or clicks or downloads or attention time,
tying funding to those seems to me to set up perverse incentives.

I want to see more people deciding consciously and cooperatively where we put our economic resources.

That also means funders shouldn't micromanage project teams.
The people dedicating their time to a project are the ones in the best position to make conscious decisions about the work.
Accountability can come from *engaged* donors giving regularly over time and deciding which projects to continue supporting
by considering whether overall progress is going well.

### Snowdrift honor system

When we first started Snowdrift.coop back in 2012,
I had just escaped from Apple and switched to GNU/Linux after years of worries about where technology was heading.
I was concerned about ads, tracking, and social media.
It all seemed so unhealthy.
I wanted to see if there was any hope for another direction.

My transition was not easy,
but I was amazed how people in community spaces like the Linux Musicians forum were so generous with their time in helping me.

I also saw unfortunate conflicts and misunderstandings.

So, I prioritized building a healthy community around Snowdrift.
Before we even invited anyone to learn about the project,
I made sure we had a good Code of Conduct,
and I collected further ideas into a healthy-communication guide.

We also wrote a community-values document which grew to cover FLO, diversity, co-op principles, and Restorative & Transformative Justice.

We even built an integrated discussion board
where anyone could flag a comment,
mark which CoC items were relevant,
and provide constructive anonymized feedback.
Then, the system would prompt the poster to edit their post to remove the flag…

*but later we had to strip that all out when the codebase got too messy to maintain…
there might be some lesson there,
something about mission-creep… I dunno…*

But these issues have always been important to me…
I actually gave a talk here at SeaGL in 2019 on restorative justice and Codes of Conduct…
the recording failed for some reason, and maybe that's for the best because I had horrible laryngitis. I could barely speak — it really was a different time then before the pandemic…

Anyway, slides from that talk and all the policies are at the Snowdrift wiki…

Just for a sense of the style, the CoC has headings like
"I will participate with honesty, treat others with dignity and compassion, respect others' privacy;
I will fix my mistakes and help others fix their mistakes…"

But I'm no longer comfortable with that CoC as is,
and I'll tell you why… 

### CLG

Just before my CoC talk in 2019, I had heard an interview with Jim Dethmer, this executive coach from the Conscious Leadership Group.

It was amazing. Greatest signal-to-noise ratio.
No small talk or prefacing, right to the point.

I often listen to podcasts at double speed, but I had to switch to regular speed, and I still kept pausing to think and take it in.

Jim talked about so many topics,
including blaming vs taking responsibility;
being motivated by fear or anger vs curiosity, play, and love;
how to make good agreements with a clear who-what-when…

At one point, Jim described sitting with his granddaughter and saying, "you're just scared, it's okay, let's just sit together and feel our feelings".
I had to pause the recording.
I sat and cried and while remembering times with my young son when I failed to have that sort of patient understanding.

So, I felt inspired to learn more.
I got their book "The Fifteen Commitments of Conscious Leadership"
and read through their blog articles and resources.

### Leadership business joke

Side note, I might not have read it if I'd just seen the cover. I'm kinda biased against anything that reminds me of generic business-buzzwords.

Now that I've read it, I think it's specific to business or "leadership" at all.
Which is partly why I now don't use the label "conscious leadership" but just "conscious living".

Of course, CLG markets their coaching to corporate clients…
and I imagine budget offices are more ready to pay for something when it's called "leadership" training…

Anyway, reading about their work really challenged some of my prejudices.

I used to believe the stereotype of Wall Street investors having 70hr work weeks, abusive office culture, superficial values, drugs and partying —
while funding exploitation, environmental destruction, and unhealthy products…

But now, I imagine business leaders practicing mindfulness, finding healthy work/life balance, and having respect and compassion for coworkers…
while they fund exploitation, environmental destruction, and unhealthy products…

To be fair, I've seen CLG grapple with deeper systemic issues.
In one blog post, they acknowledged tensions about bringing their wisdom and coaching mainly to wealthy clients
instead of to those with less resources who might need more help.

### Insights from CLG

Anyway, although many ideas were familiar to me,
I appreciated their clear style,
and some of their points really shifted my perspectives.

### Accepting constriction

One practice they emphasize is *accepting* and even *appreciating* our feelings —
especially when we feel reactive, defensive, and righteous —
even though acting from those states can lead to conflicts and problems.
CLG teaches that understanding and accepting those feelings is a necessary step before we can shift to other perspectives.

As I worked on applying that lesson personally, I reviewed the Snowdrift CoC.
I noticed that while it has a restorative emphasis on fixing mistakes,
it otherwise basically says "always be honest, thoughtful, kind, forgiving, and open-minded…".
**It doesn't deal with what to do when we feel upset and defensive…**

### Eating projections

Another practice CLG teaches is "eat your projections".
I gave a lighting talk for SeaGL 2021 on the related idea,
"preach only what you practice".

When I remember to do this, I find it really helps.

For example, I had an argument around some Snowdrift work where I got upset about how another team member was characterizing my position.
I wanted to insist that they interpret my views more charitably.
But then, I remembered to eat my projections.
I paused and asked myself whether I was interpreting *their* views graciously.
I ended up sending a message saying
"I realize I was upset and responding compulsively, and I'm sorry that I didn't take time to treat your points more graciously",
and their response was like
"oh wow, um, I thought you were going to keep defending your side, and then we'd have another extended argument, and I was all ready to fight about it more; now I feel kinda embarrassed about being so aggressive."
And we ended up resolving things easily enough from there.

### CLG's "open source"

Now, CLG practices what they preach.
In a 2018 blog post, they wrote about reviewing their values around win-for-all and abundance mentality…
and their appreciation for their influences and teachers…

And after processing their fears and hesitations,
they decided to share all their resources publicly with no paywalls or sign-up-walls or other friction.

They wrote a custom "license" in a human style (not legalese).
It's basically like the CC NC non-commercial license.
Anyone can use and adapt their resources for non-commercial use as long as they include appropriate credit.

Later, they even started using the term "open source" to describe what they were doing.

### Forking CL

So, CLG's ideas align with values I'd been focusing on for years.
And I appreciate how they collected many practices into a cohesive set.

I also saw room for improvements.
And they replied with appreciation when I alerted them to some typos and other minor issues.

So, when they ran a feedback survey,
I offered to teach them what "open source" really means and to help them actually use FLO methods.
They responded graciously but did not take me up on the offer.

So, I decided to *fork* their work and make it a real FLO project anyway.

Doing that also meant I could merge in related ideas that I had been collecting for Snowdrift.coop (which were never Snowdrift-specific anyway).

### Praveen

One thing that really helped was having a partner on the project.
My musician friend Praveen was working through some challenges with his own contexts, and I got into talking to him about CL ideas.
He ended up becoming my main coworker on the project.

Praveen has brought a lot of interesting perspectives.
For example, he wrote an alternate poetic version of my description for this very keynote:

Creative minds delight in copying,
Combining, changing, sharing as they please;
And thus do they inspire other minds,
and set the stage to then collaborate.
But people of the current world, beware!
This truth, which seers and sages always knew,
Shapes not the way that people today understand,
Or how they choose to work together or share;
Thus, the content-ocean that exists,
That's filled with countless books and videos,
Gets dumps of even more creations made
By people unaware they can adapt

### Creative adaptation

Now to be fair, *all* creativity involves copying, transforming, and combining.
That's not specific to FLO contexts.

CLG's work is directly built from specific sources and influences,
which were themselves built on others in a process going back thousands of years.

### FLO for everything

What FLO methods do is break down legal, technical, and practical barriers to this creative process.

Of course, we might worry about inappropriate harmful updates such as misinformation.
But that's comparable to not wanting malicious or sloppy updates to software.

And FLO methods include ways to deal with all these challenges.

Though it does take time, energy, and skill to do all this well.
And that brings us back to the issue of funding…

But my point for now is that just as we're happy to get bug fixes and feature updates in some program,
we can appreciate when some writings get improved in the same ways.

### FLO-CL evolution

So, what improvements have we made with the FLO-CL project so far?

Well, just setting it up forced us to clarify a lot of things.

Only some of CLG's ideas are part of their "15 commitments" and others have no clear structure.
So, we had to figure out how to organize them.

I only have time for a quick overview,
but the project includes a whole document describing the evolution.

Keep in mind that all concepts and labels are abstractions and simplifications —
mental models that are useful for thinking and communicating.

### ARR

Our new description of the main CL process has evolved so far from CLG that it's too long of a story.

But we now use: Attune-Resolve-Review.
In each moment, we can attune to thoughts and sensations.
Then, we can consider options for healthy and appropriate next steps,
and review how it went,
repeating the cycle iteratively.

### The Line → Constriction & Expansion

CLG's main model is "the line" between acceptance-and-trust vs resistance-and-threat.

In FLO-CL, that evolved into describing feelings of constriction and expansion.

We constrict in response to threats and other concerns.
And expansion relates to feeling motivated to explore, share, and create.

We can feel a mix of both at the same time, and both energies can relax.

I especially appreciate how this language emphasizes embodied feelings and is relatively free of other implications.

We now emphasize that constriction is not a *problem* and expansion isn't *better*,
it all depends on what is healthy and appropriate for each situation.
And I find this understanding makes it easier to accept whatever we're feeling.

I'm grateful for the constrictions we felt while working on this!
They led to many valuable updates.

### Context vs Content

CLG emphasizes Context vs Content
Content being our main focus during whatever activity,
and Context being the rest of what we usually don't notice:
feelings, the broader situation, the *ways* we are engaging with the content.

We decided to highlight that idea and that led us to consider other contextual topics beyond the ones they mention.

Besides Constriction & Expansion, we discuss stories, style, metaphors, traps, and habits…

### Commitments

We still have Commitments.
They are like ideals, directives, values to understand and live by.
And they have associated practices.

I won't get into explaining them here, but we'll do a rapid review of how we restructured things:

CLG's 15 commitments include Responsibility, Curiosity, Feeling all feelings, Candor… and more

Now, first… There's a missing commitment!
CLG talks about **Presence** all the time but didn't list it!

And feelings can go within that!
Also, **Candor** is part of Integrity!

Now "Don't Gossip…" What is that?
It's the only *negative* commitment.
What *should* we be doing?
Their discussion of it is actually just more ideas about candor.
But what's wrong with gossiping?
Well, mainly when it's *hurtful*.
So what we need is **Love & Compassion**!
And *that's* the commitment missing for business leaders who keep making harmful products!

And **Appreciation** fits in there!

And **Zone** fits into Integrity!

Play and Rest seems part of something bigger… **Vitality!**
And within that, we can add in all sorts of physical and mental health stuff!

Opposite-stories goes with curiosity!
These other things go with presence!
World as ally and win-for-all — those go with Love & Compassion!
Resolution, that's basically responsibility!

And BAM!
A more memorable and inspiring list that still covers everything!

Presence, Responsibility, Curiosity, Integrity, Love & Compassion, and Vitality.

So, almost done! All that's left is adapting all the teachings and practices to fit the updated structure…!
 
### Next milestones

So okay, we still have a *long* ways to go to get to anything like a v1, but we're making progress.

And I predict now that everything will change enough that we won't include anything under CLG's license.

### Invite participation 

So, we invite everyone to check out FLO-CL and participate in its evolution.

Remember, you don't need to be an expert to contribute.
I'm working on this stuff partly to improve my own practices.
Improving things as I go helps reinforce my understanding.

We could especially use help with illustrations and making a real website…

And of course, funding is a topic…
though, my real wish would be to get FLO-CL funded through Snowdrift.coop… but… uh… yeah…

Anyway, I hope this example also inspires you to apply FLO methods more widely in everything you do, not just software.

### CL needed in today's world

And I encourage everyone to learn and use CL concepts.
Imagine how much healthier our movement could be,
with less burnout and more compassion
as we build these practices into how we live and work.

In our painfully divided world,
I believe that conscious-living can be an import bridge.
We have better chances of working together across cultural and political differences
when we first establish healthy *contexts* with shared moral foundations.

And as we face daunting global challenges,
we need more than ever to consciously make healthy, appropriate decisions.

### Contact

So, again you can find the project within the FLO-Conscience group at Codeberg
(which currently has two projects, the other is the FLO-Kids repo about introducing children to software freedom, which my son Rowan presented with me here last year).

You can reach me on Mastodon, or by email or on Matrix, among other options.

### Meditation preface

Now, I'd like to end with a short meditation.

There are many styles of meditation.
And I aspire to document them in the FLO-CL project.
There's open mindfulness, there's focused concentration…
For this one, we'll do intention-setting…

So, please join me in whatever way feels right to you…

### Meditation

Attuning to the present moment…

Sitting with head tall, shoulders relaxing
Breathing slowly through the nose

Noticing thoughts and sensations, continually changing

In our time at this conference,
may we engage with one another
with curiosity, integrity, good humor, and compassion;
may we make meaningful connections
and reinvigorate one another
in our work to build technology
that supports a healthy world for all.

### Closing

Thank you.
I look forward to continuing in conversation with you through the rest of the conference and beyond.

------------------

## Draft ideas that got cut

The following notes were drafted during the talk preparation but got cut (and didn't get replaced by anything that made it in).

*Personal story of getting into FLO:*

That story involves the music business, academia, libraries, teaching, politics… and of course, software — as a user, not a programmer.
For the full story, check out my 2015 talk from Open Source Bridge called "Bringing non-technical people to the Free/Libre/Open world and why it matters"
https://www.youtube.com/watch?v=UoexwmVNmu0

But PLEASE don't ever watch anything on YouTube without a good adblocker!

*Playing with a different style:*

y'know all that FLO stuff, contributing, iterating etc; we all get this here — check out what we applied that approach to…!

*On envisioning how FLO dynamics could change how we experience things:*

There's no norm of submitting pull-requests to remove noise from a podcast or to fix run-on sentences in a book.

*On caveats and style for the meditation:*

When I planned a meditation, I noticed some minor constriction and the wish for everyone to feel truly welcomed and invited without pressure, without it feeling *imposed*.
So, I consciously explored what *styles* might fit that goal.

I had some discomfort with *imperative* language such as telling you all to "sit up tall".i
I considered variations like "let's sit up tall", "can you sit up tall?", "if you are willing and able, I invite you to sit up tall as best you can"…
then, I noticed my constriction relaxing more with plain progressive verbs, like "sitting tall".
I like how that style emphasizes an ongoing process.
And it describes an experience without presuming how or whether anyone will participate.

I also like *optative* mood with first-person plural.
That's like a sort of prayer style as in "may we fully appreciate our time together here at SeaGL…"

*More background story on the Snowdrift CoC:*

I'd heard a talk at a psychology conference about traditional vs modern college honor systems.
Traditional ones were more democratic and more central to student culture.
In their example from Randolph College, students made their own rule of no cell-phones in class.
And students took tests with no faculty supervision.
They were trusted to report issues, and juries of peers resolved conflicts and decided how and when to restore someone's standing after a violation.
By contrast, modern honor systems take a zero-tolerance approach, and the administration determines top-down penalties.
And the researchers found less cheating or other violations with the traditional honor systems.
