# Conscious Living and the Limits of Paradigms 

In 1997, systems scientist Donella Meadows wrote an influential essay called *Leverage Points: Places to Intervene in a System*. It describes 12 ways to effect change in complex systems, ordered by amount of power.

The weakest leverage point is the tweaking of constants, parameters, and numbers. In daily life, we might adjust how many calories we eat or how many minutes we spend meditating and so on.

The strongest intervention is in *transcending paradigms*. Even beyond *changing* paradigms, we can free ourselves from strictly adhering to or believing *any* particular structures or labels.

Transcending paradigms does not mean cynically rejecting or denying anything. It means accepting that our concepts do not capture the full complexity of our experiences (let alone the rest of existence beyond what we can conscious access). A view beyond paradigms is one that is more open and undefined, where we do not impose frames and limits.

We can still *use* paradigms practically. That's what these Conscious Living (CL) resources are — a set of concepts and patterns, a *paradigm* that we can use (and change) to support healthy and meaningful lives. In *transcending* the paradigm, we can keep in mind the limitations of the model and the presentation, recognizing the inherently fuzzy, rough, and generalized nature.

CL will inevitably mean somewhat different things to different people. Our experiences vary as do our understandings of them as well as the ways we communicate our understandings. Still, we can find common features and language that facilitate better understanding and communication.

## The limits of language

The structure of CL presented here is constrained by the practical limitations of text and static images (although we might eventually include audio/video resources as well, and CL discussion/support groups can provide real nuanced interaction between people). 

We do not have language for every aspect of life. We do not all interpret language the same way. Implications and meanings in language change over time and between people. So, we face risks of misunderstandings.

Acknowledging the issues inherent to the context, we aspire to minimize misunderstandings while presenting CL as clearly and accessibly as we can.

## CL provides mnemonic tools

**The CL presentation uses particular structures mainly to aid learning and memory**. Readers would do well to avoid inferring too much meaning from the structure itself. Each of us can explore, test, and adapt the ideas in whatever ways we find useful.

## CL structure notes

Putting items into files, hierarchies, and lists necessarily constrains relationships. Some of the issues with such constraints:

- When choosing where to put certain concepts, we can:
    - choose one place or the other, perhaps linking or referencing in the other place
    - split the concept into aspects that go in each place
    - repeat ideas in multiple places
- Some examples of awkward distinctions:
    - Principles vs Practice: these inform each other and overlap
    - The different aspects of *context* and the different commitments all interact and overlap

## Traits over types

In CL, we prefer structures and concepts that bring flexibility. Discussing ideas, people, and situations in terms of *traits* provides more flexibility than categorizing everything and everyone into *types*.

Throughout CL, we can consider whether something has *elements* or *aspects* that fit notable patterns. CL avoids hardline definitions about whether something *is* or *isn't* a particular category. We can instead ask *how much* of various traits something has.

For practical simplicity, we may use structures and labels that seem to imply types. However, when we investigate any type of thing, we can still see it in terms of traits. We can even consider the *traits of traits*…

## CL is non-essentialist

We see no reason to believe the essentialist idea that types of things have some core, foundational basis for their identity. When we investigate and observe and ponder aspects of life at all sorts of scales, we do not seem to find ultimate end points to anything. Of course, we can notice distinctions and patterns, and we can use labels. We can still see the labels and concepts as mere pragmatic traits in how human thinking happens to work.

## CL is adequately agnostic

By simply experiencing consciousness, we can know with utter certainty that it is real. Can we know anything else with that level of confidence? Perhaps mathematical facts?

Mostly, we come to accept various ideas through predictive reliability (seeing repeated validation of falsifiable predictions) or from intuitive comfort with explanations and stories that seem to fit our experiences. We need not hold such beliefs in a rigid, fixed style.

In CL, we avoid making claims that ask for deferential acceptance. Practical wisdom and knowledge does not require faith. Everyone can test and review CL concepts through direct experience in applying the ideas.

---

## CL leverage points

Though Meadows' original *Leverage Points* essay focuses on ecosystems and environmental sustainability, the ideas apply to all sorts of systems. The best understanding comes from reading the full essay thoroughly rather than skimming the headings. See <https://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system>. That said, the Wikipedia article has a decent summary: <https://en.wikipedia.org/wiki/Twelve_leverage_points>.

Many people misunderstand these points as a simple list from worst to best ways to engage with a system. A better understanding really uses the leverage-points metaphor. As with a physical lever, the points that require the least effort only have much effect if the amount of change (the distance the lever is moved) is great. Other points require much more effort, but then small moves translate to much greater effect.

In any specific case, the choice of where to focus depends on the particular details and our practical capacities. Any point can make a difference, but if we only make *small* changes to the *easiest* leverage points, very little will change.

We can consider how CL itself fits the leverage points framework. This way, we can consider where to put efforts when presenting, developing, and using CL itself. In order from least to most powerful (easiest to most difficult):

- **(12) constants, parameters, and numbers**: We can consciously adjust our specific agreements such as how much time we spend on particular activities or how much of what food to eat at each meal.
 
- **(11) buffers**: we can set up our habits and agreements to maintain buffers in our time availability and energy levels, leaving room to adapt to changing circumstances.
 
- **(10) flow structure**: CL considers our shifting energy patterns, especially in terms of constriction and expansion, arousal and relaxation; we can intentionally set up our life to adjust the structures that influence our energy flow.
 
- **(9) delay time, relative to rate of change**: CL highlights an efficient process of attune-resolve-review that works at all scales. This pattern makes CL adaptive and able to respond quickly to emergencies while avoiding rushing other decisions.

- **(8) adjusting negative feedback loops**: CL emphasizes listening to and accepting our *constrictive* energy which impedes and inhibits us. We can appreciate when our constrictions reduce harmful patterns and keep us safe, and we can learn to reduce overreactive constriction when it gets in the way of healthy patterns.
 
- **(7) adjusting positive feedback loops**: CL highlights leading by example, preaching only what we practice, and continually reviewing. CL also brings awareness to *expansive* energy which motivates various actions. We can use these patterns to help reinforce healthy feedback loops and relax unhealthy ones.
 
- **(6) structure of information flows**: CL supports the use of measurement tools as well as coaching, collaboration, and community practice; CL includes methods for how to live with integrity, including appropriate candor, considering when and how to reveal ourselves to others.
 
- **(5) rules of the system (including incentives and constraints)**: CL introduces important foundational principles, commitments, and practice tools. The commitments hold us to high standards.
 
- **(4) the power to change and evolve**: CL embraces FLO (Free/Libre/Open) dynamics and processes to support continual improvement of our life patterns and of the CL resources themselves.
 
- **(3) changing goals**: CL reframes our goals to align with healthy and ethical ways of living.
 
- **(2) paradigm-shift**: CL itself offers a transformative contrast to many common worldviews and mindsets. CL also emphasizes that we can change the stories we tell about our lives.
 
- **(1) transcending paradigms**: as highlighted above, we can acknowledge that no evolution of CL structure, language, and presentation can ever make it perfect and complete. And the same applies to any potential substitute or alternative paradigm. We can let go of analyzing and structuring everything, allowing everything in consciousness to flow as an ineffable process.
