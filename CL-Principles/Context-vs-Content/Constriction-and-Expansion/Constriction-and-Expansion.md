# Constriction and Expansion

In CL, we use constriction and expansion to refer to both direct physical body sensations and metaphorical patterns in thoughts.

## Constriction

Constriction means narrowing. In CL, we use constriction broadly to refer to feelings of closing, limiting, blocking, and holding.

Constriction relates to all forms of defense, protection, inhibition, and so on. Threat-reactions like fight-flee-freeze-faint are constrictive. Our airways constrict to block pathogens and pollution. Our whole bodies constrict to shield us from violence. Constriction can mean locking our doors and building fences and walls.

Less-reactive constrictions include everyday cases of saying "no", setting boundaries, and stopping. Constriction can mean protecting our privacy, and it can mean claiming ownership of property. Constriction can look like withholding our thoughts and feelings from others, clinging to our judgments, and resisting change.

Consumption is constrictive. Getting, taking, eating, and capturing are all constrictive actions.

Conservation can also be constrictive — whether we're conserving traditions or ecosystems or resources. While simply consuming less is not constrictive, it *is* constrictive to *actively* protect something by inhibitng, resisting, or blocking consumption.

## Expansion

Expansion means spreading out. In CL, we use expansion to refer to feelings of both opening and growing. 

Expansion relates to exploration, speculation, going, and so on. Expansion can mean feeling excited to develop new ideas, to learn, and to teach. Expansion can drive us to promote our passions to all the world. 

Moderate everyday expansion includes saying "yes", feeling willing to try things, and being kind to others. Expansion can mean greeting people, collaborating, and sharing. Expansion can look like writing thoughtful messages, asking open questions, and working for progress.

Production is expansive. Giving, creating, and helping are all expansive actions.

## Related terms

Although we default in CL to using "constriction" and "expansion", many related terms exist and have value.

While constriction means tightening, contraction means shortening. The opposite of *contraction* is *extension*. We could play poetically with pairs like *contraction* versus *extraction* and *contention* versus *extension*.

When we focus on being *impressive*, we might do an outward performance, but our main energy is focused inward on how we are judged. When we focus on being *expressive*, we send energy outward toward giving, sharing, and connecting.

Basically, constriction is *closing*, and expansion is *opening*, and we can use those words directly. Consider common phrases like closed-minded and open-minded. We could also say narrowing and widening. Not all synonyms come in regular pairs. 

Some useful contrasting pairs (among many other possibilities):

- constriction / expansion
- contraction / extension
- contraction / extraction
- compression / expression
- compression / extraction
- impression / expression
- income / outcome

- Various **constrictive terms**: close, contract, compress, clench, cramp, cringe, choke, grasp, grab, capture, catch, hold, hoard, take, own, claim, constrain, confine, conserve, limit, temper, tense, restrain, restrict, reject, resist, depress, suppress, stifle, forbid, exclude, obstruct, avert, avoid, block, prohibit, inhibit, doubt, stop, but, no

- Various **expansive terms**: open, extend, express, grow, develop, progress, blossom, bloom, flower, shine, glimmer, glow, radiate, emanate, transmit, communicate, reveal, convey, publish, advertise, announce, become, manifest, activate, exhibit, trust, go, and, yes

In everyday communication (with those not familiar with CL concepts), some language works better than others. Strangely, "constricted" seems more common than "constrictive", but it's the other way around for "expanded" versus "expansive". Using the terms along with gestures can help. We might describe expansion with phrases like "feeling like *going* and *doing* things!"

## Constriction and expansion as a continuum

The simplest model for constriction and expansion is a one-dimensional continuum. Like a magnet, the poles exist as aspects of a unified system. As we get closer to one pole or the other, it affects how much we feel each.

**constriction** <--------------------> **expansion**

## Energy levels

A better model shows the two types of energy coexisting without being on a zero-sum continuum. We can have low-levels of both or high-levels of both or some other mix.

A 2-dimensional graph works well:

![Diamond-shaped graph with the bottom left side labeled "constriction" and the bottom right side labeled "expansion", each with arrows pointing up. A vertical line on the side labels up direction as "burnout" and the down direction as "rest"](constriction-expansion.png)

Side-note: These dimensions compare somewhat with a common mapping of emotions in psychology using *valence* and *arousal*. Negative valence means averseness, pain, bad. Positive valence means attractiveness, pleasure, good. While those "hedonic tones" seem similar constriction and expansion, they do not quite match. In CL, we see constriction as energy that can apply to both aversion and attraction. We constrict to avert some things and constrict to cling to other things. Expansion has its own qualities that do not necessarily connect with pleasure and attraction.

By considering *energy level* as a distinct dimension, we can see how **releasing or relaxing constriction is different from increasing expansion**. Releasing the brakes is a different action than pedaling forward. Adding expansion to constriction does not bring peace, it brings distress. Braking hard and pedaling hard at the same time is a recipe for breakdown and burnout.

Both constriction and expansion can be *resolved*. In the ongoing rhythms of life, we can talk of ***tension* and *resolution***. "Tension" implies constriction, but that can also refer to a sort of constrictive sense that often goes with expansive energy. We can feel expansive pressure, a sense of compulsion to *do* expansive things. In many cases, the release of tension is what *allows* expansion to happen. So we can talk of resolving any sort of energy, even though resolving constriction may be more prototypical.

- Various terms related to increasing energy: energize, excite, amplify, reinforce, stimulate, arouse, motivate, drive, compel, obsess, urge, boost, provoke, prompt, pressure, rouse, rile, spark, trigger

- Various terms related to decreasing energy: relax, release, relieve, resolve, rest, pause, slow, settle, calm, quiet, still, ease, loosen, unclench, allow, accept, free, let go, liberate

Note that some energy terms can imply constrictive or expansive directions. Trigger is most used in constrictive contexts, while spark is usually expansive. Also "open" as a *verb* fits the idea of expansion well, while "open" as an adjective also implies relaxation.

## Not good or bad!

Whether expansion and constriction are [healthy and appropriate](/CL-Principles/Attune-Resolve-Review/Healthy-Appropriate.md) depends on how they fit the circumstances. So, they are *not* synonymous with good and bad or healthy and unhealthy.

Healthy constriction protects us and provides important signals. Constriction means limits and conservation. Without constriction, we can't *hold it together*. If we stopped reacting to threats, we would soon be dead. It's unhealthy to be so open-minded that our brain falls out. We will not survive long being open-minded about touching hot stoves, getting hit by speeding cars, or drinking poison. We will not thrive by being open to abuse, exploitation, or neglect.

It is not because pleasure is right and pain is wrong that we seek one and avoid the other. Seeking pleasure and avoiding pain are inherent to what the feelings are for. To thrive, we need to take pleasure in healthy things and feel pain when experiencing unhealthy things. It will not serve us well to reduce pain by eliminating our pain responses. When we get injured, we need pain to motivate us toward appropriate responses and learning. Appropriate responses address the underlying causes of pain, avoiding and treating diseases rather than only suppressing the symptoms.

Healthy expansion drives us to change the world for the better, to spread love, make art, to connect with others. Without expansion, we would never produce any value.

Expansion can also be unhealthy. Expansion accompanied by delusion can mean insanity. Unbounded growth is cancer. Complete expansion is entropy — the end of coherence and form.

Pessimism is constrictive, optimism is expansive, and neither is objective or *correct*. A pessimistic view can feel discouraging, while an optimistic view can lead us to skip important precautions. We need to attend to problems in order to fix them, and we need to notice things that are going well in order to appreciate and sustain them. Sometimes, we need to relax our judgments, predictions, and expectations in order to be open to just allowing and experiencing whatever comes.

One strongly constricted state is *depression*, and the corresponding expanded state is *mania*. We are unhealthy when stuck in either state or when flipping between them as in bipolar disorder. A balanced life moves and flows more smoothly, modulating through a healthy range of appropriate energy levels.

## Forms of constriction and expansion

Life is a process of constriction and expansion. Heartbeat, breath, and movement… Our energy continually fluctuates. Every topic, assertion, idea, and experience can be felt with constrictive or expansive energy, and parts of us can feel differently at the same time.

For example, constriction around the topic of love could mean craving love or rejecting love. Part of us can feel expansive, open, unconditional love for others while another part of us feels constrictive *fear* of the pain that may come from losing those we love. We can even feel expansive energy around pain — welcoming pain in a way that involves curiosity and appreciation, free from any indulgence in unhealthy self-punishment. As with soreness following strong physical exercise, we can appreciate the pain of losing a loved as being wholly part of the love itself. We would not wish to feel no grief.

### Nuances of wanting and wishing

When we *want* something, we usually sense that as constrictive. *Desire* implies craving and grasping. The word "wish" often feels more nuanced because it can include acceptance that the idea may not come about. A *wish* for world peace can feel expansive when we experience it through love and compassion rather than through defensive grasping.

When we have "hope", do we sense some constriction, some clinging to a that future we can only accept if it goes the way we prefer? Do our *hopes* for good health come with some constrictive fear and trepidation about how things might go badly? Maybe "aspiration" feels more expansive. We can *aspire* to live by healthy commitments in a way that feels free from resistance to whatever may or may not happen.

Whatever the language, our constrictive or expansive feelings show up in raw sensations. Still, we might actually feel different when we use different language.

### Wanting to get or to get away from

Aversion and clinging are *both* constrictive. We can want, grasp, hoard, crave, indulge in, and hope for pleasant experiences. We can resist, push away, avoid, fear, and criticize unpleasant experiences. Although these do feel  different, they are two sides of the same coin. They are both forms of constriction.

Expansive energy is more *giving* and playful, more curious. We may want to write a song, travel to a new place, or share a meal with friends. In these cases, we may feel only expansive energy pushing us to action. Or we may feel mixed…

### Layers of constriction and expansion

Complex behavior can involve both expansion and constriction. For CL practice, it can help to ask which energy is the core motivator. For example, hunger drives us to seek out food. The seeking is somewhat expansive. Then, we eat the food, and that's constrictive. The overall behavior of getting and eating food is driven by constriction.

Attachment to any aspect of experience is itself constrictive. We can even feel layers of *constriction about constriction*. We begin to relax when we *accept* our constrictions, when we drop resisting our resistance. Again, the goal is not *ending* constriction but *noticing* it, heeding its messages, and then responding appropriately with contextual awareness and best healthy practices.

For example, consider teachings about consciously letting go of worldly attachments, clinging, and greed. We hear about accepting the truth of impermanence and accepting discomfort. Many of us take those ideas and then constrict around the *attainment* of freedom and equanimity. We feel aversion or shame toward greedy thoughts and feel greedy about achieving progress in conscious living. This pattern is commonly called "spiritual materialism". We might take the instruction to "let go of clinging, let go of resistance" and then constrict around actually doing it. The problem is in the implied goal of ending constriction as if it were bad. To avoid this pitfall, we can note the topic of "progress in conscious living" and see how we feel around that, accepting whatever we notice, including any constriction. When constriction shows up in layers, releasing it may happen layer by layer (though there are also practices that may quickly just relax everything all at once).

## Continually shifting rhythms 

Life inherently fluctuates. Everything is always changing. Trying to stay in expansion is as absurd as trying to only exhale.

Like inhaling and exhaling, constriction and expansion are opposites and have distinct effects. Still, they are similar processes with some similar characteristics. They go together.

We cannot release constriction through force. Fighting against constriction only adds constriction. Scratching an itch can make it worse. Forcefully blowing a constricted and inflamed nose will only make it more irritated and inflamed. A smooth flow through constriction and expansion comes from acceptance, relaxation, and healthy regulation.

As with breathing, we can exercise and explore constriction and expansion. We can refine our sensitivities and learn to quickly notice subtle signals. We can also challenge ourselves, developing our capacity to tolerate tighter constrictions and reach broader expansions. We can pause temporarily and sit in a *relatively* stable state for a time. We will always have subtle fluctuations, because nothing is truly static. Eventually, we will go on to experience the next overall phase in the continual patterns of life.

Patterns of shifting energy will continue until we take our last breath (and even after that, though presumably without our conscious awareness).

## Appreciating perspectives

Having multiple perspectives is essential to fully understand any situation. What appears when viewed from constriction? From expansion? From a more relaxed state?

When we experience freedom and relaxation, we can stay present with that state as long as it lasts, or we can consciously shift toward constriction or expansion to explore those experiences.

Our views from constriction and expansion will be reliably *different*. With no single *correct* perspective, the greatest insights come from exploring as many angles as we can. We can start with subtle, mild challenges. To safely experience and process extreme views and states, we can use careful precautions and preparations. As our skills improve, we can work effectively with greater challenges; ever more profound and troubling questions and feelings; and growing creativity, love, and compassion.

