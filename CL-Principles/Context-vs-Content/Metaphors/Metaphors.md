# Metaphor

In CL, we use "metaphor" to refer to the expanded concept of [*conceptual metaphor*](https://en.wikipedia.org/wiki/Conceptual_metaphor). That means acknowledging how both linguistic and nonlinguistic thought patterns involve making *connections* between different ideas. Such connections are a dominant part of human experience.

CL includes the practice of using metaphors more consciously, explicitly acknowledging them and choosing appropriate metaphors with careful intention. Conscious presence can involve recognition of metaphors, relaxing the connections in order to notice thoughts and sensations more directly.

## How basic concepts connect metaphorically

Both the simplest words like "bright" or "green" and abstract terms like "govern" have direct meanings. Many basic words refer to core sensations (sight, sound, taste, smell, pressure, pain, hunger, heat, balance, and others). We have words that describe ranges of patterns in experience such as feelings (sadness, fear, disgust, comfort, etc). We use other basic words for actions and objects in our direct experience (jump, sing, tree, bicycle, filter, and on and on). We have many spatial relational experiences we describe with prepositions (like in/out, up/down, with/without, before/after, etc) as well as comparisons like more or less. We also use simple labels for many big abstract ideas (family, chemistry, music, conversation, future, and more).

Note that concepts are not mere semantics. Words and other references are only one of multiple ways we think and communicate. Both basic concepts and metaphorical connections occur outside of language as well. Given this explanation itself being in words, that's that focus here.

When we form words into phrases, sentences, and longer, we activate our sense of all the words and concepts in conjunction. This process is so common and constant, we do not notice it. Many people only talk about metaphors explicitly for unusual, startling, novel cases such as in formal poetry.

## Metaphor ubiquity

Everyone constantly uses metaphor throughout our discussions and thoughts. *Foundation* is a metaphor in the statement, "words that refer to direct sensations provide the foundation for abstract metaphorical thinking." It is as though we have a physical structure, a building, and some words and ideas are the foundation which hold up the other ideas.

Metaphor is so constant and prevalent that we cannot practically analyze all of it any more than we can consciously attend to *all* of our thoughts and sensations. If we stop to notice all the metaphors, we will not get through much of any communication or thought process (notice the "get through" metaphor, as if thoughts are a substance to move through like we would wade through a swamp).

### Active metaphor vs etymology

When particular metaphorical phrases become especially ingrained in language, they become like new basic words, and we can lose any connection to the original distinct ideas. In CL, we take only mild interest in the etymology of cases like that. Metaphors matter when they do indeed involve activating together multiple concepts that have independent meaning for us.

## Metaphors affect our understanding and attitude

Since metaphor means activating concepts, words, images in conjunction with each other, *which* things get activated can significantly affect our experience. Changing our metaphors can change our intuitions, our preferences, and our mindsets. Different metaphors can completely alter our thought patterns and our interpersonal communication.

## Metaphor choice in CL practice

Simply understanding the prominance of metaphor in our thinking enables us to bring conscious intention to the issue. Choosing metaphors wisely can be one of the most powerful tools in CL practice. Exploring metaphors creatively can bring new insights and perspectives.

## Limitations and pitfalls of metaphor

Metaphors are useful as long as we don't believe them. All metaphors have limits and risks. We ought not take them too literally. Recognizing metaphors enables us consider them critically, noting where they bring insights and where they bring confusion.

## Notable CL metaphors

The main use of metaphor in CL is the concept of constriction and expansion. We connect that to many related ideas. It helps to bring various forms of wanting, resisting, exploring into awareness of core bodily sensations.

We have also collected some other metaphorical images as useful frames to consider during CL practice. For example, a [stream flowing down a hill](The-Stream.md) as a metaphor for our flow of experience.

