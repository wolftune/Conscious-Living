# The Metaphor of Reflection 

We never see ourselves directly. An eye cannot see itself, and a camera cannot photograph itself. We only view ourselves indirectly through mirrors and recordings.

We can notice reflections as being what they are. We can notice the distinction between direct view and reflection.

Other energy besides visual light can reflect too. Sound reflects with echos and reverberations. Waves in air and water reflect. Different energy and different objects cause different reflections. 

People use reflection *metaphorically* for all sorts of ways that we consider and make sense of things. When we bring memories to mind, we experience them like reflections. Meta-awareness, our thinking-about-thoughts, is like bouncing imagination off a mirror in order to see it from a distance.

## Seeing reflections in a window or lake

In many settings, clear glass windows reflect some light while also letting light through. We can focus our eyes on the objects beyond the window, or, at the right focal distance, we can see our reflection in the glass. In the right conditions, the same context can arise with still water. We can look into a lake and see underwater objects or we can notice the reflections.

We can use this metaphor for all cases where we see limited perspectives. We can think we've gotten all the views when we see both near and far outside the window. We may miss seeing the window itself, and we may miss seeing our reflection in the window.

We might think it takes a lot of practice or effort to see a new view. We might hear unclear, unhelpful instructions like, "the image is right before you". We could spend enormous time looking through the window, hoping to see it.

How we explain reflection makes a big difference. The bad instruction "look deeply into the water" might lead to never seeing a reflection. Better teachings might involve adjusting the lighting until reflections are easier to see. We might hold up a mirror against a window and then take it away while keeping the same eye focus.

When we see a reflection, we just see it in an instant, in a *flash*. It's not something we have to strive for or build up or struggle to get. It doesn't develop slowly over time.

## Distorted reflections

Constricted or expanded mindsets distort our views like curved mirrors. What we see is not complete hallucination. The reflections are of real things. The image is just distorted. We see things squished or stretched. In extreme cases, what we see is squished, stretched, flipped, discolored, cracked, and dirty.

We can learn to understand our cognitive distortions in the same way that we can learn the patterns of how different mirrors produce different distorted reflections. By testing different mirrors and viewing from different angles and in different lighting, we can make sense of our experiences.

We can also learn which mirrors and lighting are most useful when studying and investigating. In some cases, we use complex combinations of mirrors and lenses such as in cameras, telescopes, microscopes, and so on. Choosing the best tools and knowing how to use them can bring us profound insights. We can also enjoy and laugh at interesting distortions without confusion about what is going on.

With modern technology, we can see things "reflected" through computers that tweak, clean, adjust, and do more radical adaptations of images.

The "reflections" of our memories also distort as our imaginations fill in missing pieces or adjust over time.

Can we find a neutral enough perspective to reflect in the manner of a clean, flat mirror? Without dirt or distortions, a flat mirror's reflections can be almost indistinguishable from a direct view…

## Mirrors and self-awareness

People use mirrors as a test of self-awareness. The classic mirror-test checks whether someone directs attention to their own body when seeing a mark on their reflection in a mirror. Only a few types of animals have clearly demonstrated this style of self-recognition. They include apes, elephants, rays, dolphins and whales, and some birds. Human children tend to show this recognition during their 2nd year.

The process of learning to identify with our reflection involves a sort of metaphor. We connect the ideas of "self" and "me" to the image in the mirror. One sort of presencing practice involves releasing this metaphor, noticing again that we never actually see our own face.

## Active listening as reflection

When we communicate with others, *reflection* can mean the ways a listener shows their understanding. Most directly and simply, they can *mirror* the speaker, showing their attention. Listeners can also *echo* (a delayed reflection) by simply speaking back exactly what they hear the listener saying. Beyond that, listeners can send back variations, interpretations, and questions.
