# Story triangles

*TASK: illustration with flipped triangles, red one labeled "conflict", green/blue labeled "growth" with notes about "fear" vs "trust"*

A common story dynamic features three character archetypes in a triangular relationship:

- Victim
- Villain
- Hero

This makes for a conflict-drama with the victim as helpless, lacking agency, at the effect of the actions of the villain (the bad guy) and hero (the good guy).

A more empowering dynamic reframes the characters:

- Learner
- Challenger
- Coach

In that growth-story framing, the learner has agency and responsibility. The challenger and coach support the learning, and are not in opposition. There are no good-guys and bad-guys, only challenges, failures, and successes.

Fear drives us to see things in terms of conflict while trust helps us to see situations in terms of learning and growth.

Note that, as with all stories, these dynamics can be internal patterns (different parts of us) as well as separate people in a social context, and individuals can shift roles around the triangles.

## Victim vs learner

Victims generally feel helpless, powerless, hurt. Attention focuses on pain, suffering, and defeat. Victim mindset goes with complaining thoughts like "I can't", "it's too hard", "it's hopeless", "I have no choice", "help!", "nobody understands", "it's not fair", "why me?". Experiences in victimhood often include getting sick, distracted, overwhelmed, and feeling ashamed.

 Learners feel curious, unstuck, and focused on choices they have. Attention goes toward CL practices and commitments. Attention is on curious questions and doing one's best in the circumstances. Learners engage *playfully* with situations, even when uncomfortable. Learners question their beliefs and stories and are willing to say "I don't know".

## Villain vs challenger

Villains attack, criticize, and condemn. In subtle and internal contexts, the prototypical villain behavior is *blame*. Villains have thoughts like "you're wrong", "I/you/they should…", "I don't care", "get over it", "so stupid", "waste of time", "it's their/your/my fault", "#$&* you!". Villains often make excuses for their behavior like "it's just a joke", "I didn't mean it", "you made me do it", "you deserve it". Ironically, it is villainous to focus on labeling someone (or some part of us) as the *villain*, as the enemy.

Challengers make things difficult *in order to* push the learner to rise to the occassion. Challengers may add discomfort. Challenge is often unpleasant. Challenge still can be an essential part of learning and is not our enemy. Challengers adjust the level of difficulty to what learners are ready for in each moment.

## Hero vs coach

In this model, a hero provides *temporary* relief. Heroes may banish villains, relieve discomfort, and bring in pleasant feelings. Heroes take care of problems without teaching anyone how to deal with situations in the future (besides calling for the hero again). Heroes typically see themselves as superior to the awful villains and poor victims. Heroic thoughts include "I'll take care of this", "I understand", "let's all get along". Heroes tend to listen with a focus on fixing problems. Internally, heroic actions can include procrastination, addictions, distraction, and so on. Heroes also often overreact by making drastic decisions all at once.

Coaches support learners in finding healthy and appropriate resolutions with iterative steps that serve both immediate and long-term concerns. Coaches respect other roles as equals and partners in the learning process. Coaches listen fully and openly. Coaches serve as facilitators and guides, creating a context for learners to make their own discoveries rather than telling them what to think.

Coaches guide learners through CL processes with questions like "what can you learn from this?", "what is at risk here?", "how else could you see this?", "what sensations do you feel?", "what sound and movement would express your emotions?", "what do you appreciate about this situation?", "how is this serving you?", "what support would be most helpful?", and so on.

## Constrictive conflict vs expansive learning

The conflict-triangle goes with feelings of *constriction*. The learning-growth-triangle goes with *expansion*. Presence helps us to shift from reactive conflict to creative, aligned learning.

---

*Adapted from ideas that evolved from the work of Stephen Karpman, then David Emerald, also the Hendricks Institute, and then the Conscious Leadership Group*
