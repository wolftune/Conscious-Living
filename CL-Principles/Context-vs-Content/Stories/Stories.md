# Stories and Beliefs

Stories are so important to human societies that some have proposed the name *homo narrans* (storytelling human) instead of *homo sapiens* (wise human). Cultures are largely defined by histories and myths. A popular story says that storytelling itself is what enabled people to coordinate in groups beyond the size where everyone could know one another directly. Today, billions of people believe stories about money, government, corporations, science, and culture. Without these shared stories, much of life as we know it would not happen.

Because of the strength of stories in human thinking, we easily exaggerate the significance of anecdotes, and we have a hard time letting go of stories except by replacing them with alternate stories.

When our stories *change*, so do the ways we behave. By noticing, deconstructing, and playing with stories, we can see their impacts, their power, their utility, and their pitfalls. With conscious awareness of stories, we can choose and adapt our stories intentionally. We can use helpful stories while still recognizing them as mere stories.

## What are stories?

One form of story is a simple account of events. A plain story might simply describe things in the form of "X happened, then Y happened, and then Z".

Most stories have settings, characters, feelings and motivations, conflicts or challenges, decisions, resolutions, and other common features. We usually tell stories from particular viewpoints and with particular styles. Stories often follow common patterns which people classify with thematic labels like adventure, mystery, romance, growth, morality, and tragedy.

In CL, we focus on *traits over types*, so we can discuss the features of stories without getting hung up on exactly what counts as a story or as what class of story. We can note *how much* any given story includes or implies various features or how much it follows certain patterns.

## Stories and memory

Connecting events into a narrative sequence greatly serves our memories. We use stories in education and in keeping and sharing cultural, familial, and personal histories. Stories are among the most effective strategies to remember enormous amounts of information. People can memorize random lists (such as objects, numbers, or orders of shuffled playing cards) by tying each item to an event in an adventure story (such as imagining seeing 15 cats on a porch and then going inside the house to discover a table with 21 salt shakers and so on). We can remember and recall stories more easily when we find them surprising, outlandish, and emotional.

Side-note: what is memory? Broadly, "memory" means that after an event, something stays changed. Memory does *not* require consciousness. A book flipped over and then returned to original position has no memory of the event. A book getting wet and then drying out has a memory of the event (the paper will never fully return to its original condition). When things revert after a small delay, we call that *short-term memory*. When things never revert, we call that *long-term memory*.

In CL, our focus is on those memories accessible to conscious awareness — what is called *declarative* or *explicit* memory. CL deals only *indirectly* with procedural memory (such as motor skills).

Note that CL deals as directly as we can with *sensory memory* — the lingering experience of sensations following the events that trigger them. Maybe we can consciously experience the sensations without any noting or labeling or stories. However, to talk about and think about sensations, we must consciously note them in *some* form, and that involves declarative memory.

The forms of declarative memory do not all involve language. We can avoid words and concepts and just remember some ineffable mental image of of a past sensation. Also, declarative memories can be conscious or subconsciously. We bring memories to conscious awareness through *recall*.

### Concepts vs stories (semantic vs episodic memory)

Psychology describes declarative (explicit) memory as *semantic* or *episodic*.

Semantic memories include *concepts* such as mathematical calculations, explicit procedural instructions (like recipes), language, and abstract ideas. Semantic concepts can have simple labels like "freedom", "creativity", "death", and so on; or they can be specific constructions like "attune, resolve, review" or "the six CL commitments". Beyond language, we have semantic concepts such as our mental image of the look of a prototypical tree or the taste of banana.

Semantic memories are separated from any specific memory of a particular time and place. So, semantic concepts do not *feel* like stories or story implications. Of course, we can *use* semantics as the basis of stories, and we tend to understand and teach semantics *through* stories. One way we build semantic concepts is through statistical-learning. We can experience enough varied cases of something that we get a general sense for the pattern divorced from any specific instance.

*Episodic memory* is the foundation of stories. Perhaps the language of "stories" is just a more colloquial way of talking about the mental processes related to episodic memory. Episodic memories normally show up as images, some form of replaying of experiences. This sort of story-consciousness is the emphasis whenever we talk about stories in CL.

## Implied and assumed stories

Keep in mind: *traits* over types. We can see semantic vs episodic memory as a spectrum. The more a thought brings up ideas of places or times or events or characters, the more story-like we experience it. Even within semantic concepts, we tend to have more story-sense for ideas like "The United States of America" or "World War II" or even "school" than for more abstract ideas like "inventory" or "episodic memory".

A short statement can often *imply* a story that it might be a piece of. The assertion "I can't do it" can prompt us to imagine a story with a character, a challenge, a point of view, a setting, all within some broader context.

The less detailed a story, the more room we lave for varied interpretation, imagination, and potential misunderstanding. The more thorough and specified a story, the more robust and precise it can be — which can also be limiting.

## Nonfiction vs fiction

For nonfiction, stories stick to what people have actually observed. Still, most nonfiction describes events without acknowledging the observation process. "He traveled to Paris" is a more common style than "his letters describe traveling to paris" (or the extreme of "I [the author] recall reading in his letters that he traveled to Paris").

Nonfiction still usually adds embellishments, commentary, interpretation, and imagination on top of and around the observations. Nonfiction can imply different things from what is or is not included in a story.

A story veers into fiction when "he traveled to Paris" is changed to "he traveled to Moscow". With fiction, we are free to imagine anything at all. We can change the basic events or even imagine different universal physics or mathematics. In practice, most fiction sticks to settings and contexts based on real-world observations. We imagine particular details of events and characters within realistic situations.

Most stories blend fiction and nonfiction even when they lean one way or the other. In CL, we aim to stay conscious of our use of fiction and nonfiction.

## Stories can be reversed and changed

When we see how the same observations and concepts can fit different stories, it helps us relax our hold on particular story. When we feel free to play with stories, we can consciously explore which versions achieve the most healthy and appropriate effects.

Even with the constraints of nonfiction, we can flip around play with stories. We can change the style, the perspective, include or omit different parts, and so on. We can adjust any imagined aspects of the story that are not constrained by direct observations.

Clearly, a story changes if we switch "I can't do it" to the opposite of "I *can* do it". But we can make more subtle adjustments as well. A situation that brings up "I can't do it" might accommodate alternatives like "I can't do it alone" or "it's really difficult" or "I have never done it before".

We can also test stories for coherence. People hold a surprising amount of internally contradictory stories. Logical inquiry is one way to investigate and loosen the hold of particular stories.

Exploring and presenting multiple varying stories helps us avoid overconfidence in any particular interpretation. That doesn't mean all stories are equally useful or insightful. Concepts like parsimony help us to consider which stories best fit our observations. Still, consciously noticing our deference to parsimony or other guidelines can help us avoid overconfidence even then.

As long as we honor our direct observations, no version of a story is strictly the "right" or "true" one. A *shared* story might or might not be the story that aligns best with our feelings and intuitions.

### Playing with fiction and nonfiction

Playing with stories can bring us valuable insight and perspectives. Non-fiction still has a lot of room to explore how we tell stories. Fantasy and fiction helps us further see how stories affect our feelings and decisions.

A story with just observations might be "I felt nervous, and they rejected me." From that we might notice the implication, "I was nervous, and that's why they rejected me". We could then flip that to get "I was nervous, but that's not why they rejected me". We could also loosen the story as "I was nervous, and maybe that's why they rejected me". Going into fiction, we can flip the story completely: "I wasn't nervous, but that's not why they accepted me."

## Stories, predictions, and hypotheses

When we make predictions, we often do so in the form of stories. We say, "if I stay up late, I won't have enough energy for my normal morning routine," and we imagine all sorts of unspoken details about how the next morning will feel. This story implies some causation or at least a strong correlation. We hold a story in mind that we must go to bed on time in order to feel rested and energized in the morning.

We can alternatively express the prediction as a more precise hypothesis with less storytelling. We can say, "I noticed that after nights where I stayed up late, I didn't complete my next morning routine, and I predict that pattern will continue." We might inquire about the causes with curiosity. Could it be that breaking our bedtime agreement sets up a pattern of not respecting our agreements, and *that* is the cause of failing to do the morning routine? What other explanations could we explore?

When we build skills in storytelling, imagination, and especially *altering* our stories, we can more easily come up with diverse hypotheses. We can then discover patterns and relationships that we miss when we simply believe the most accessible or common stories.

In science, researchers collect data through observations. When they then write journal articles, they go beyond reporting their observation process and the collected data and calculations. Usually, they add stories about their observations. They discuss interpretation, speculations, and explanations. When done well, they mark the stories with clear and explicit distinction. Unfortunately, such clarity is usually lost once the research gets shared in popular media.

## Self-fulfilling prophecy

Our stories profoundly affect how we feel and what decisions we make. Some of this fits the idea of *belief effects* like *placebo* (when believing that something will heal us leads to healing) and *nocebo* (when thinking that something will harm us brings real suffering beyond the distressing thought itself).

We tend to behave as we expect the characters in our stories should behave. We even tend to get from others the behaviors that we expect from them. The stories we tell set us up for confirmation-bias (where we particularly notice whatever observations fit what we already believe and disregard anomalous observations).

As we consciously play with our stories, we start to see how things could be different. That frees us to choose and play-out alternative stories. Our new stories can be just as self-fulfilling. Thus, conscious storytelling is a way to choose our direction.

Still, the effects of storytelling are limited. A story that we can fly like a bird will not keep us from falling and smashing to the ground. And consider the aphorism, "it is easier to *act* our way to a new way of thinking than to *think* our way to a new way of acting". Stories do not define everything.

## Common story patterns and elements

Some of the aspects of stories to notice and explore: theme, plot, style, setting, characters, archetypes… People have made various lists, usually focused on how to write the most common sorts of entertaining stories. Storytelling is not limited to these nor does it require any of these features strictly, but noticing these patterns can be helpful.

*Plot* is the basic sequence of events. Often, plots focus on some sort of *theme*, typically around a change such as conflict and resolution or learning and growth.

*Setting* refers to the context — where, when, and the overall state of the surroundings within which the story takes place.

*Characters* in a story have various features, patterns, descriptions, looks, and attitudes. They play various roles and have relations to one another. Often, characters fit common *archetypes* such as victim, villain, hero, bystander, teacher, partner, parent, boss, leader, stranger, fool, friend, enemy…

Stories can have features of various *genres*, following general patterns of *action*, *adventure*, *tragedy*, *comedy*, *rebirth*, *mystery*, *fantasy*, *romance*, and so on. Some stories focus on *redemption* or *achievement* (overcoming challenges and things getting better) while others tell of *contamination* or *destruction* (how things worsened, what went wrong).

Stories may use *poetic justice* where "good" triumphs and "evil" gets punished. Or stories can leave us with more complex and uncomfortable understandings where things do not necessarily work out for the best or are left inconclusive and unfinished.

Stories are told from particular *perspectives* or *styles* such as first-/second-/third-person, past/present/future, active/passive voice, and much more.

Consciously noting these story elements supports us in exploring and playing with them and considering the effects they have.

## Stories at different scales

We can tell stories at many different scales. Stories can be:

- *intrapersonal*, focusing on different parts of us and patterns we have over time with different moods and feelings
- *personal*, focusing on describing one person's character and experiences
- *interpersonal*, focusing on dynamics between people
- *societal*, focusing on patterns across large groups of people
- *non-personal*, focusing on patterns in physics, cosmology, biology, and so on — often told with personification, making characters out of non-personal elements

We can also tell stories at different time scales. The scope can be a few moments, a day or week or month, a lifetime, or across thousands or millions of years.

## Stories and worldviews

We tend to hold general perspectives — worldviews — based on abstract stories. Such stories frame our religions, political movements, moral judgments, and philosophical stances. 

The influence between stories and perspectives goes both ways. For example, we hear stories that describe people as naturally good (versus naturally evil), and then we see the world from that perspective. However, we also more readily accept stories that validate our intuitions.

In CL, we aim to notice our worldviews and paradigms explicitly and to find willingness to explore and question them.
