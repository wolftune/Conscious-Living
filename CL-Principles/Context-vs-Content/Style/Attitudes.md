# General Attitudes

Style includes overall attitudes/approaches we may take, whether temporarily in a particular moment or context or habitually and across different contexts.

People have made many attempts to simplify attitudes into models like Enneagram or shorter lists of supposedly primary dimensions like the Big Five Personality Traits (conscientiousness, agreeableness, neuroticism, openness, extraversion). The [Wikipedia article on Trait theory](https://en.wikipedia.org/wiki/Trait_theory) describes just some of the ways psychologists classify traits.

Simple personality models all seem to trap people into fixed mindsets where we try to fit everyone into the model. They get us to ask who *is* which *type* (this person is a "helper", that person is an "achiever" etc.) or who *is* high or low on a trait (he is introverted, she is extroverted, and so on). Rather than leading to better understanding of one another, fixed identity labels tend toward confirmation bias and self-fulfilling-prophecy. We might notice all the supporting evidence while discounting or missing nuances that challenge the model. If we see everyone in these simplistic terms, we might change how we treat each other, and people might then respond in ways that fit the expectations. And making the models ever-more complex to capture more nuances still doesn't escape the inherent problems with using fixed, static labels.

When we remember that attitudes can change, we can stay more curious about ourselves and others. Though we have persistent patterns and habits, even apparently stable life-long traits could be circumstantial — given that some circumstances can be life-long as well. We can imagine but can't *really know* how someone would change if circumstances change. And even when we get to see such changes, we cannot fairly separate the influence of the prior context from ingrained personality. Across different contexts, our personas can vary remarkably and can have seemingly contradictory traits.

We can still use common labels to consider patterns. We need not think of the labels we use as a *complete* set. A giant and somewhat-unstructured list of attitudes can help us avoid the trap of overconfidence. With a long enough list, we can forget about keeping so many descriptions in mind at once. And we the idea of type-groups doesn't work when the number of possible trait combinations is inconceivably large.

The adjectives listed below are useful prompts for investigating and considering our mindsets. The collection draws from all sorts of descriptions of personality traits, mindsets, and other characterizations — freed from the context of rigid models.

The list has pairings *roughly* following [constriction vs expansion](/CL-Principles/Context-vs-Content/Constriction-and-Expansion/Constriction-and-Expansion.md), but they are not to be taken strictly, and some attitudes really do not fit that framing. Also, these are not black and white dichotomies and could often be alternatively listed as multiple terms along a spectrum. 

Note that even with explicitly negative vs positive styles, we cannot say generically whether a negative or positive style is *healthy and appropriate*. That determination must be done in context. Often, contrasting traits are *complementary* — bringing all the different perspectives to a situation brings the most insights.

Some various attitudes and general styles:

- self-focused vs other-focused vs together/group-focused vs universal-focus
- helpless vs empowered
- analytic vs holistic
- difference-focus (splitting, diversifying) vs similarity-focus (lumping, unifying)
- focused & narrow vs unfocused & broad
- competitive vs cooperative/collaborative
- negative vs positive
- solemn vs humorous
- staid vs silly
- attentive vs distractable
- obsessive vs apathetic
- compulsive vs free-wheeling/nonchalant
- detached vs engaged
- precise/clear vs vague/enigmatic
- perfectionism vs pragmatism
- maximizing vs satisficing
- avoidant vs participative
- passive-observance vs active-performance
- planning-composing-reflective vs spontaneous-improvising-impulsive
- routine-reliable vs experimental-unpredictable
- subtle-restrained vs romantic-expressive
- crass-vulgar-brash-rude vs polite-gracious-courteous
- blunt vs tactful
- challenging-critical vs agreeable-gregarious
- contrary vs complementary
- unhelpful-unsupportive vs helpful-supportive
- cruel vs kind
- dismissive vs sympathetic
- antagonizing vs peacemaking-mediating
- insulting vs flattering
- modest vs boastful
- humble vs proud
- stingy vs generous
- vengeant vs forgiving
- diligent vs lazy
- persistent/tenacious/insistent vs yielding/relenting/deferential
- pessimistic vs optimistic 
- cynical vs gullible
- doubtful vs confident 
- slumped vs poised
- loyal-dutiful vs fickle-flaky
- obedient-submissive vs rebellious-assertive
- conforming-assimilating vs eccentric-nonconforming
- stubborn-tenacious vs adaptable-accommodating
- disciplinarian vs nurturing
- dependent vs independent
- cowardly vs courageous 
- guarded vs vulnerable 
- shy vs friendly-outgoing
- private vs public
- hoarding vs sharing
- aggressive vs passive
- controlling vs yielding-deferential
- hyper vs relaxed
- hesitant vs eager
- neat-organized vs sloppy-chaotic
- strict/rigid vs lenient/flexible
- conscientious vs nonchalant
- careful vs carefree
- cautious vs daring
- minimalist vs maximalist
- plain-simple-sparse vs decorative-flamboyant-ornate
- concerned vs apathetic vs oblivious
- default inclination toward fight vs flight vs freeze vs fawn…
