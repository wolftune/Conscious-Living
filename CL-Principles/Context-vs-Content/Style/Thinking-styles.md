# Thinking styles

This document collects various models and ideas about thinking styles.

These concepts work best when we avoid the trap of simplistically typing people as one or the other style of "thinker". Articles on thinking styles too often get into asking questions like "what's your thinking style?" (or in some cases "learning style"). *Thoughts* have style, and people may tend to use some styles over others, but thinking style is like tone of voice. Though we can characterize the different styles of different voices, we don't mark people as being speakers of particular tone categories. We know that everyone varies their tone depending on context, mood, intention, and so on.

We all use a mix of thinking styles. And when we increase conscious awareness of them and get interested in their different values, we can use and adjust our styles appropriately.

## Overall cognitive styles

Common classifications in psychology use labels like:

- abstract: emphasis on general ideas, concepts, symbols
- concrete: emphasis on precise objects, events, and experiences
- analytic: focus on details, isolated study of parts
- holistic: taking a big-picture, global, and continuous view
    - Note that full holistic thinking is not even really *describable* because language itself comes from a more analytic perspective
- systems-thinking: focus on patterns of relationships and interactions, how parts within systems go together and affect one another
- creative-associative: making connections between ideas, as in metaphor; developing novel formulations
- perceptual: emphasis on sensations and sensory images
- spatial: thinking of positions and orientations in space, as in maps and similar
- categorical: emphasis on distinguishing types from one another
- critical: emphasis on evaluating, testing, questioning
- convergent/linear: using common (or at least pre-set) strategies
- divergent/lateral: seeking and using uncommon or novel strategies and perspectives
- discursive: thinking in conversational forms
- metacognition: thinking about thinking

Related to creative, associative, and divergent/lateral thinking is *possibility* thinking versus *probability* thinking. Naturally, familiar ideas are more accessible and salient. It is all too easy to ignore, avoid, or discount unfamiliar or unlikely ideas. Consciously focusing on what is possible can open us to explore greater potential options.

## Thinking in different sensory modalities

Thoughts come in different forms: language and numbers, sound and music, kinesthetic/tactile, visual, and other sensory modes. All the raw sensations we experience can also be brought to mind as thought images. In addition to the commonly-described senses, we can indeed *imagine* proprioception (being upside down, dizzy, balanced, falling, etc), pain, temperature, and all other sensations.

Our thoughts may come as more clear, specific images or more fuzzy, generalized images; and the clarity might vary across modalities. We might emphasize or favor some modalities over others. Thoughts of language can be experienced visually as writing, auditorily as word sounds, and kinesthetically as speech-production movements.

## Thought flow

Patterns of thought can flow freely or cycle in obsessive loops, and thoughts can be scattered or more focused. 

## Thinking in models

We have no direct access to some ultimate reality. We conceive of the world through conceptual models. So, much of our thinking comes from the ways we structure our models. While the actual details of our models affect how we think, so do our [*styles of modeling*](Model-styles.md), such as thinking in traits or types, continuous spectra or discrete elements, and more.

## Cognitive distortions or clarities

The "cognitive-distortions" described in Cognitive Behavioral Therapy (CBT) all relate to thinking styles. As styles, they are not *necessarily* distortions. For example, there are cases where filtering is healthy and appropriate and improves understanding. These styles are distortions *when* they are *inappropriate* and lead to misunderstandings. And we can contrast *distortions* with generally healthier *clarities*:

- All-or-nothing / black-and-white thinking vs spectrum/continuum thinking and balanced thinking
- Minimizing, magnifying, and filtering vs attuning to direct observations and full context
- Jumping to conclusions (including overgeneralizing, mind-reading, fortune-telling, and more) vs conscious wondering and hypothesizing, cautious and qualified concluding, appropriate agnosticism, and conscious consideration of probability and uncertainty
- Shoulding vs coulding and questioning
- Personalizing vs detaching
- Blaming vs responding
- Righteousness vs curiosity

Various lists of cognitive-distortions are longer or shorter. Oten, the items in the longer lists are variants or combinations of the basic set above.

## Other thinking-style models

Executive consultants Mark Bonchek and Elisa Steele made a model describing big-picture vs detail-oriented thinking, each with four different focuses: ideas, process, action, and relationships.

Psychologist Robert Sternberg describes thinking styles with a governance metaphor:

- functionally: legislative, executive, or judicial
- formed like: monarchy, hierarchy, oligarchy, or anarchy
- focus: local or global, internal or external
- leaning: liberal or conservative

Again, as long as we understand such models as imperfect guides, they can be useful to help us explore different styles.
