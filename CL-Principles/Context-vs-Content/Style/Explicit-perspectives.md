# Marking perspectives explicitly
 
Taking the time and care to explicitly note our perspectives can add important clarity and help avoid overconfidence and misunderstanding. We can state the precise context from which we hold an idea, such as:

- "I notice…"
- "I recall…"
- "I think…"
- "I predict…"
- "I imagine…"
- "I read…"

Sometimes it is helpful to avoid the first-person and use more neutral wordings like "here's a thought…"

This sort of explicit marking helps leave room for alternative perspectives. Contrast "You're ignoring me!" and "I think you're ignoring me!". The latter asserts only the truth of having the thought, leaving open the question of truth in the claim within the thought itself.

## Separating observation and imagination

When describing our perspective, it helps to distinguish [observation and imagination](/CL-Principles/CL-Commitments/Presence/Observation-vs-Imagination.md).

When recounting our observations, we might add qualifications like, "I read that…" or "he told me…". How far to go with this pattern is a practical question. "It was 3PM" marks no perspective. "I saw the clock showing 3PM" makes the observation explicit. It would normally be unhelpful overkill to say "I now have an image in mind of a clock showing 3PM, and it feels like recalling a memory of a real experience, so I imagine that I did indeed see it." That said, deconstructing our perspectives that way has a place in CL practice, though we do not recommend it as a normal habit.

Note that explicitly marking the perspective of the observer can make it easier to recognize observation errors such as illusions.

## Imagination prompted by observation

We constantly imagine all sorts of things around our observations. A straightforward observation could be: "your voice got much louder". We often bring judgments and interpretation, such as: "you yelled angrily at me". Further imaginations brings us to: "you yelled at me in anger because you're jealous that I took a vacation while you stayed here to take take of everything".

Separating observation and imagination (with some added questioning tone) looks like this: "I noticed your voice getting louder, and I imagine you're feeling angry. I'm guessing that you feel jealous of me for taking a vacation while you stayed here to take of everything. Is that right?" Note that the tone of voice here can have a huge impact on whether these words come across with compassion and understanding rather than dismissal or accusation.

Of course, a plain observation could include saying, "*you said that* you are jealous of me taking a vacation while you stayed here to take care of everything" — if it indeed describes a direct observation *of a person saying that*. Depending on how much marking and qualification we imagine to be helpful, we can further preface, "I heard you say that you are jealous…" or even "I recall hearing you say that you are jealous…"

We can appropriately adjust the language for describing imagination. Different people and circumstances will call for variations like:

- "I imagine…"
- "I'm guessing…"
- "I'm picturing…"
- "Is it…?"
- "It seems…"
- "I think…"
- "The story I have in mind is…"

## Tradeoffs for concision

All this perspective-marking does make statements longer. We need not apply these patterns pedantically. Much of the time, we can safely leave perspective simply implied. However, people habitually fail to recognize our limited perspectives and to imagine the possibility of other perspectives. So, marking perspectives explicitly is often worth the extra conscious time and effort. Shifting our thought and communication patterns can shift our mindset. And sometimes, explicit marking makes all the difference in avoiding unnecessary tensions and conflicts.
