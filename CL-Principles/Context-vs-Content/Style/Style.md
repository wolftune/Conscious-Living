# Style

Style refers to the *way* or manner in which we do things or think about things. We can talk about the style of our language, our thought patterns, body posture, and so on. We can also say that particular things use or are in certain styles.

Both directly and metaphorically, one main way to think of style is in our physical **stance, posture, attitude, approach, inclinations…** Style also shows up in the *form*, shape, structure, arrangement, design, and method of how we organize and express ideas. Style is in our ***tone*** of voice. We use various writing styles, speaking styles, listening styles, and so on.

Though we associate styles with feelings and meaning, they are not the same. Fear is not a style. Fear is an emotion. We might describe a shaky, nervous voice as having a fearful style; and it might trigger or exacerbate our feelings of fear; but the style is in the expression, the shakiness itself. Style is in the difference between saying "my foot hurts" and "I feel pain in the foot", but style is not in the sensation of pain itself.

## Effects of style and playing with styles

The same overall message expressed in different styles can have dramatically different results. 

Style can make the difference between emotionally wrapped up in a topic or viewing it neutrally with emotional distance. We may constrict in reaction to some styles and not others.

Consciously using styles that are not habitual can bring us new perspectives on a topic.

All style choices have advantages and disadvantages, benefits and risks, pros and cons… Though we can emphasize particular features of different styles, CL calls for us all to use conscious exploration and investigation for ourselves. We can consider style choices and apply the attune-resolve-review process and take interest in our constrictive and expansive feelings.

## Personalities as style tendencies

We all have many ways of being, feeling, thinking, and acting. What people call "personality" is the conglomeration of our relatively persistent and stable style patterns. And along with our overall tendencies, we also have multiple *personas* — ways that we show up differently in different contexts.

We can wonder how far styles can change over time and can vary among our personas. Some traits do seem ingrained — perhaps driven by genetics or set in early childhood, but it can be difficult to test how much is fixed or variable. Styles can change with changing contexts and with dedicated reinforcement of new styles.

Too often, personalities are described as limited sets of fixed categories. But possible combinations of traits are far too numerous to fit a few strict categories. Perhaps some traits do correlate, but the evidence for strong correlations seems weak. Attempts to define small sets of personality boxes all seem unhelpful and unconvincing. Better to simply note the various style *traits* of any particular persona.

## General attitudes

Much of style can be described in terms of overall attitudes like generous, confident, aggressive, lazy, and so on. These are abstract judgments open to a lot of interpretation. Still, they imply a good amount. We can consider whether we approach a situation with an overall aggressive or timid mindset or with humility rather than pride and so on.

The main framing for our state at any time is [constriction and expansion](/CL-Principles/Context-vs-Content/Constriction-and-Expansion/Constriction-and-Expansion.md) and overall energy level. Different styles lead to different ways of filtering, processing, and showing constriction and expansion. We can sometimes classify various nuanced adjectives in terms of constriction and expansion, though they can also vary by different contexts.

See the [list of general attitudes](Attitudes.md).

## Thinking styles

We can think in different styles such as analytical or relational thinking or emphasizing different forms of thought such as language or sensory images. We can focus for extended times or allow our minds to wander and daydream. We can pay attention to some sorts of topics more than others — style as patterns of tastes and preferences…

There are many different aspects to thinking style and different models to consider. See the dedicated article on [Thinking Styles](Thinking-styles.md)

## Language styles

We can describe the same overall topics, the same *content*, with an astounding variety of language styles. Different languages use different styles (something particularly accessible to those who speak multiple languages). Within any language, there are many different dialects. Within any dialect, there are many options for grammatical form. We can choose different synonyms or describe things negatively as *not* the antonyms. We can make assertions or ask questions. All these and other language style variations have effects on how we think, feel, and communicate. 

To reiterate the basic concept of conscious-living: Considering language style in CL means bringing conscious awareness to the different styles and the feelings they elicit and the results they have. No aspect of language style is outside of CL consideration.

A collection of books on language style could fill a large library. So much has been written or said, so many opinions, so much research, entire careers focused on language style… We have a document that covers as much as we practically could compile toward exploring the wide scope of language styles from a CL perspective: see [Language Styles](Language.md)

## Voice, tone, and music

Aside from wording choices, all the other aspects of speaking have major effects.

The timbre of our voices can range from whispery to shaky to steady, mumbled or clear and precise, rounded and warm or edgy and sharp.

Our prosody (the flow of pitch and rhythm, intonation, groupings, and stress) can change the very meaning of words. We can speak with low or high pitch, more flat or more varied pitch, quietly or loudly, slowly or rapidly, continuously or with pauses. We can stress different words or phrases. We can chunk (group) words in different ways.

Our speech delivery can include more or less ums and ahs, laughter, or "likes" and "y'knows". We can also use other non-linguistic vocal expressions. We can scream, yell, yelp, cry, sob, whine, wail, whimper, grumble, sigh, coo, "hmm", "mhm", and more.

All of these variations of voice and tone can indicate our emotional state, our level of confidence or certainty in the ideas, sarcasm and jokes… Tone and flow also indicate things about how others might engage or not in conversation (such as when and whether they might speak up, take their turn, or interrupt). Tone can be the difference between interruption feeling dismissive and condescending or understood as respectful clarifying questioning.

Of course, speech can be more or less musical. Singing is a particular sort of vocal expression. Of course, music is itself an extremely broad topic mixing many features we won't get into here. Different musical styles can provide very different emotional contexts for voices and well as for other media.

In written language, tone can be *implied* through emoji, punctuation, capitalization, and other indicators. However, these signifiers are not the same as real tone, and most writing does not indicate tone at all. **The lack of tone in written language is a major source of misunderstanding and lack of emotional connection between people.**

## Kinesthetic styles

We can hold and move our bodies in different styles, and we have habits of how we use our bodies in different contexts. Our movement styles have profound connection to emotions — influences going both ways with emotions leading to movement styles and movement styles changing our emotions. We also interpret much from what we observe in the bodies of others.

We can be stiff or loose, static or mobile, jerky or fluid, poised or slumped, slow or fast, subtle or extreme. We can lie, sit, crouch, squat, stand — and do many variations of each.

Our gait can vary in terms of foot-strike (forefoot, mid, or heel), amounts of pelvic rotation and tilt, flexion, and more. We can stroll, amble, shuffle, saunter, strut, power-walk, skip, hop, march, jog, run, and sprint. With accessories, we can skate, pedal, surf, glide, and more. Of course, there are countless and widely varying styles of dance. We can jump, spin, sway, kick, shake…

We also have styles of interpersonal physicality. All sorts of handshakes, high-fives, fist-bumps, hugs, and kisses. We can have more or less personal space. We make or avoid eye contact. We can synchronize our movements and our breathing. These all have profound impacts on our feelings of connection. Of course, we can also do violence, and that too comes in many styles.

Alongside verbal communication, we have all manner of gestural styles and facial expressions…

## Visual styles

Visual styles may be noted in terms of colors, lighting, forms that are more hard-edged and deliniated versus fuzzy and blended, straight or curved, plain and simple versus elaborately decorated, and so on.

Visual styles show up in a wide range of places in our lives: clothing, other body decoration (makeup, tattoos, jewelry, hairstyles), architecture, design and decoration of spaces both indoors and outdoors… There are photography and video styles; styles of drawing, painting, sculpture, and so on; and writing has typography (different fonts, spacing, sizing, etc).

## Styles with other senses

Are there styles for other senses? Maybe. Food style involves visual presentations and kinesthetic aspects to how we eat, but there's obviously also *flavoring* and *texture* styles which are about taste and touch. Clothing style also goes beyond visual in terms of texture, fit, and weight.

We use scent stylistically by using products to adjust the odors of spaces, objects, and our bodies.

Can temperature be part of style? We might serve different foods hotter or colder; dress more or less warmly… Maybe we can talk of style in the patterns of how we heat and cool buildings and whether we bathe in colder or hotter water.

## Media styles

Technology has brought us many different means of recording, expressing, sharing, and observing. We have relatively fixed media like audio and video recordings, text, and illustrations. We also have dynamic and interactive media like live conversation, computer programs, and more. Each medium has its own internal styles and we can consider the choice of media itself as a stylistic decision. Different media have different strengths and weakness and other characteristics. See the full article on [Media Styles](Media-styles.md).

## Story styles and narrative structure

We consider story a separate sort of context from style. What happens, what the characters are like, what they do… that stuff is the *content* of a story — and the story is itself part of the *context* for any particular focus within it. But of course, we tell stories in various styles.

Story styles use the various styles mentioned here. However, some parts of story style do not fit the other categories. Story style is not part of language style because we can tell stories without language. For example, we can tell stories purely visually as in video or live mime performance.

One aspect of story style is narrative structure. In what *order* do we present a story? Do we use clear linear order? Do we jump around? Do we group parts thematically? Do we end abruptly, leaving things unresolved or open to imagination?  Many structures and approaches are possible.

One important structural style: Do we say **what before why** or the other way around? So, do we foreshadow and preface before revealing main ideas? Or do we state key points up front and then explain them? Stating claims before caveats (what before why) can make ideas more accessible and easier to follow, but sometimes setting the stage first can reduce the risk of misunderstandings.

## Task, tool, and design styles

We can see styles in the manners in which we do all sorts of tasks and activities, the tools we choose, and the way we use our tools. All sorts of things and specialties have their various styles. Inventions and constructions have stylistic elements — choices among the multiple options and forms that serve the essential functions.

## Social-organizing styles

Beyond direct communication and other interpersonal connections, we have indirect patterns in how we organize communities and overall societies. We have many different ways to make group decisions and coordinate our actions. In different times, places, and aspects of societies people have used a wide variety styles of governance, styles of family structure, and so on.

## Association, prejudice, and playing flexibly with styles 

Like everything else in life, we never experience style in isolation. There is no style outside of style *of* something. As with [metaphor](/CL-Principles/Context-vs-Content/Metaphors/Metaphors.md) overall, we build connections between styles and whatever we experience in conjunction.

People tend to associate styles with particular people and with different cultures and subcultures. We also connect our own personal styles with particular feelings. These associations then affect how we interpret or misinterpet the styles we observe in others.

Can we limit such associations? Can we experience styles in a more direct way without prejudice? Do styles have fundamental energy that we can pick up on directly? For example, are sharp corners and straight lines in visual art *inherently* aggressive? Are certain hairstyles inherently rebellious?

Surely some aspects of style are shared by all people. Everyone can agree that some styles have more or less energy. And we all experience styles *relative* to the broader context. In contexts where boldly expressing feelings is normal, yelling may indicate mere mild frustration. In a context where feelings are usually suppressed and expressions subtle, the same yell might indicate a rare and extreme explosion of profound rage.

Of course, many stereotypes have a basis in truth. Facial hair is associated with greater testosterone which does affect styles of thought and behavior. But so many other factors influence any particular person and situation. And when we engage with prejudice, it drives our styles which then affect the responses from others. We cannot easily break these influences apart.

Becoming *conscious* of our tendency to see things through associations is a first step toward a more open view. Then, we can consciously *play* with style, altering various features. By putting a style in *many* different contexts, we can weaken our associations and experience the style more neutrally. Similarly, by trying many different styles within any particular context, we can better free the context from our stylistic prejudices and associations.

## Tone-policing vs conscious style resolution

Different styles can be more or less healthy and appropriate for different circumstances. And the same is true for our *style* of bringing up style issues!

Sometimes, people complain about style feedback that they see as blocking or ignoring their core message. "Tone policing" refers to using style requirements to *filter* out messages and perspectives. Often the filters limit messages that express constriction. If only calm or expansive people get to speak, then the people most upset about something (perhaps for good reason) get their voices stifled. In other cases, style filters might favor native-language-speakers or people with certain cultural backgrounds or education and so on.

To support healthy styles without the problems of tone-policing, we need to make *some* appropriate time and space to hear messages in all sorts of styles and to process our reactions consciously. We can notice when we constrict in reaction to some styles and not others. We can then express our constrictions explicitly. Resolving such issues involves *some* mix of (A) adjusting our reactions, staying calm enough ourselves to hear the message despite the style and (B) helpfully supporting others in adjusting their style to help the communication go better. Sometimes, it helps to change the context, such as to communicate privately or in a different medium or with assistance of a facilitator.

When choosing styles, another can pitfall show up when we prioritize tactful, careful, stoic styles. Subtlety and politeness can *hide* feelings. Like a trojan-horse, style choices can lead us to bypass the feeling and expression of *appropriate* constrictions that would otherwise help us notice dangers and problems…

As a rough guideline, it is healthy to care about styles, to feel constriction around using inappropriate styles. Instead of insisting that any style be accepted or that only a strict set of styles be accepted, we can see how appropriateness varies across all sorts of contexts. Some styles might help us process feelings privately, others are better for communication. Some styles work better with some people or when people are in particular moods. When we explore styles consciously and invite others to do the same, we can use style skillfully and adaptively while honoring the CL commitments.

## Homogeneity and diversity, standards and deviation 

One broad element of style is in how we approach questions of standards and diversity. We can emphasize consistency and norms or creativity and novelty.

Suggesting that some styles are good or bad brings a risk of encouraging homogeneity, of everything ending up relatively similar. Sameness can feel simple and comforting, and it can feel limiting and even oppressive. Diversity can bring freshness, vitality, and broader perspectives; though it can also bring challenges.

Places filled with identical houses and square monoculture lawns have much less vitality compared to neighborhoods with a mix of building types, varied gardens, and a mix of people and wildlife fitting into the different niches. Such mixing does also increase the chances for conflicts — though that can itself lead to learning and healthy outcomes when resolved well.

We can't say that diversity is always healthy. A lot of benefits come from making and respecting appropriate standards. Consistent sizing of nuts and bolts enables repairing and reusing. And, y'know, we don't get healthier with more diversity of diseases or of torture methods.

When we consider the styles across different contexts and content, how varied or not are they? Styles themselves can range from stable and consistent to dynamic and contrasting. In a situation filled with varied, dramatic styles with changing colors, many different materials, and continual adaptations; everything can feel similar in being complex and fluctuating. Ironically enough, bringing a static and plain style into that context would add diversity and contrast.

With practice, we can cultivate healthy style habits that we continually update and adapt consciously. And how we approach diversity is one of the many factors to consciously consider.
