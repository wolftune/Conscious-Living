# Context vs Content 

Wherever attention focuses in any moment, we experience it within a broader context. *Focus* is itself a *visual* reference. Our eyes fixate and focus within a bigger visual field. The same happens with all forms of thoughts and sensations. Attention emphasizes some parts of conscious experience while other parts remain more peripheral.

Another phrase for *content vs context* is *figure and ground*. Within a scene, we treat some elements as *foreground*, focusing on them as the main features. Other aspects we treat as *background*. Foreground/background itself uses visual-spatial metaphor: what is in front of us or in back of us. What we metaphorically *face*, where we point attention, that becomes the *figure* in our experience.

## Attending to context

CL emphasizes bringing conscious awareness to contexts. Besides asking *what* we experience as the main content of a situation, we can ask *how* we experience it. What else is happening around alongside and around our focus? Within what attitude, mindset, mood, settings, feelings, perspectives, and beliefs do we experience things?

To explore context, we bring a topic to mind and then observe the rest of what shows up along with it. The process is like fixating our vision on something and then shifting attention to the surroundings in our peripheral vision.

## Notable contextual features

The primary context for CL is consciousness. So, one aspect of context is whether we are conscious of context itself! Are we *aware* of how we treat different parts of experience as content or context?

We have also noted some other important contextual features.
The main one we consider in CL is [constriction and expansion](Constriction-and-Expansion/Constriction-and-Expansion.md). Other contextual features include [style](Style/Style.md), [stories](Stories/Stories.md), [metaphors](Metaphors/Metaphors.md), and more.

We can also observe patterns of context. While we can potentially come to any topic with any mindset, some topics and framings are more or less likely than others to bring up constrictions or other contextual patterns.

## Shifting content and context

Because we can focus attention on anything within consciousness, we cannot say absolutely what is content and what is context. We could focus on a relationship and then notice feelings of stress as part of the relationship's context, or we could focus on stress and see the relationship as the context for the stress.

Once we notice what we see as content versus context, we can explore and play. We can leave the focus in place while widening our awareness, and we can change our focus. A complete flip of focus is called a *figure-ground reversal*.

Although anything can get focus, there are predictable patterns to what topics we normally emphasize and what we tend to leave in the background. A life-coach may treat CL topics as central content, but for most of us, CL ideas are *contextual*. We attend to them as we process the other main topics of our lives.

## Layers of context and content

Although we can only really attend to a limited amount at one time, we can conceive of a whole scene as being the content for a still broader context. An immediate task is part of a bigger project, and the project is part of a general area of life which itself is in the context of our overall existence. At the smallest levels, our next thought or sensation fits into the context of just-prior events. As our attention moves around, we can mark things for later focus.

## Experiencing the contrast

Content vs context is a *subjective* experience. We impose these views on the world. The distinction is real to the extent that the experience is real. No objective distinction exists independent of our experience.

The duality of content and context, figure and ground — these are opposite sides of the same coin. We understand them in relation to each other. We understand day in contrast to night. Yin and yang are different yet inherently intertwined. There is no up without down, no back without front. Still, front and back *feel* different… 

### Non-dual experience

One style of mindful presence involves releasing the figure-ground distinction, learning (as best we can) to take in everything at once with no highlighting. Some call that experience *non-dual* because the sense of separation disappears even though our views remain dynamic and varied. We no longer distinguish content from context, but we still see complexity (it's not like everything blurs together and loses form).

At a meta level, we can see separation of figure and ground as itself just another aspect of the totality of conscious experience. Just as other thoughts and sensations arise, so does the sense of figure-ground distinction. When we take focus away from any of these things, we can tend to imagine a distinct self observing everything, and then we may treat this self-image as the figure. Full non-dual experience can arise when even that sense of a distinct observer drops away.

Can we (ironically enough) experience non-duality in different ways? Can we see everything as ground versus everything as figure? Can we see everything as context to nothing or everything as content with no context? Language can break down trying to describe experiences of unity or nothingness. People may talk of no-self or self-as-everything. Do these different perspectives describe different conscious experiences?

## Practical views

Although important perspective comes from experiencing non-dual presence, we cannot practically deal with everything at once. We use figure-ground/content-context distinctions in the normal processes of making sense of the world, making decisions, and acting.

The CL resources provide guidance as we explore the context of whatever topics arise in our lives. With more conscious processing of context, we can achieve more healthy and effective resolutions of the main content.
