# Noting Observation vs Imagination

As we note our conscious experience, we can mark a distinction between observations and imagination.

## Describing observations

Observation starts with the sensations we directly experience. We see, hear, touch, taste, smell, and so on.

When we mentally note or communicate about sensations, we rely on abstractions like words. Still, we can use language that points as directly as possible at the raw sensations. We can do that by describing the details of making observations themselves.

We can say, "I see [description of what we see]" or just "seeing [description of sights]". 

The words we choose to note sensations impose concepts and interpretation. The concepts are themselves thoughts and are not the same as the raw sensations. That said, we can consciously hold the markers lightly and acknowledge that they are rough, imperfect designations that go with the full ineffable experience. We can intentionally choose language with the least amount of implications and added interpretation.

## Imagination

Some people use "imagination" mainly for creative fantasizing. We think of imagining fictional things that have never existed like dragons or interstellar travel or simply fictional people having conversations in realistic scenarios. 

In CL, we understand "imagination" to cover even likely real-world things that we do not observe. A family member goes shopping and comes back with groceries, and we *imagine* them going to a specific store, putting groceries into their bag, and so on. Our imagination might nearly match direct observation from the past, but we still have only imagined and not observed what happened this time.

Imagination is usually the closest we can get to seeing someone else's perspectives. We observe someone's face, and we directly experience our own reactions (which may include empathetic feelings and even matching the facial expression of others). We do not actually observe *their* feelings. We only *imagine* what their subjective experience might be. We *observe* frowns and cries; we *imagine* someone's sadness. 
